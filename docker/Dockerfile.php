FROM php:8.2-fpm

ENV COMPOSER_HOME /tmp

RUN apt-get update && apt-get install -y \
        git \
        libpq-dev \
        libzip-dev \
        unzip \
        libicu-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libwebp-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-configure gd --with-webp --with-jpeg --with-freetype \
    && docker-php-ext-install -j$(nproc) intl gettext pcntl zip pdo pdo_pgsql gd \
