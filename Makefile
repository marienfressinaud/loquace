.DEFAULT_GOAL := help

USER = $(shell id -u):$(shell id -g)

ifdef NO_DOCKER
	PHP = php
	CLI = php cli
else
	PHP = ./docker/bin/php
	CLI = ./docker/bin/cli
endif

.PHONY: docker-start
docker-start: ## Start a development server with Docker
	@echo "Running webserver on http://localhost:8000"
	docker-compose -p loquace -f docker/docker-compose.yml up

.PHONY: docker-build
docker-build: ## Rebuild Docker containers
	docker-compose -p loquace -f docker/docker-compose.yml build

.PHONY: docker-clean
docker-clean: ## Clean the Docker stuff
	docker-compose -p loquace -f docker/docker-compose.yml down

.PHONY: db-setup
db-setup: ## Setup the database
	$(CLI) system init

.PHONY: db-migrate
db-migrate: ## Migrate the database
	$(CLI) system update

.PHONY: db-reset
db-reset: ## Reset the database
ifndef FORCE
	$(error Please run the operation with FORCE=true)
endif
	rm data/migrations_version.txt || true
	$(CLI) system init

.PHONY: help
help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
