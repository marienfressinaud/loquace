// Allow to disable the automatic scroll-to-top on form submission.
// Submitting forms with a `data-turbo-preserve-scroll` attribute will keep the
// scroll position at the current position.
let disableScroll = false;

document.addEventListener('turbo:submit-start', (event) => {
    if (event.detail.formSubmission.formElement.hasAttribute('data-turbo-preserve-scroll')) {
        disableScroll = true;
    }
});

document.addEventListener('turbo:before-render', (event) => {
    if (disableScroll && Turbo.navigator.currentVisit) {
        // As explained on GitHub, `Turbo.navigator.currentVisit.scrolled`
        // is internal and private attribute: we should NOT access it.
        // Unfortunately, there is no good alternative yet to maintain the
        // scroll position. This means we have to be pay double attention when
        // upgrading Turbo.
        // Reference: https://github.com/hotwired/turbo/issues/37#issuecomment-979466543
        Turbo.navigator.currentVisit.scrolled = true;
        disableScroll = false;
    }
});

// Hide / display long content
function initExpandable() {
    const elementsExpandable = document.querySelectorAll('[data-expandable]');
    for (const elementExpandable of elementsExpandable) {
        const elementHandle = elementExpandable.querySelector('[data-expandable-handle]');

        const elementScrollHeight = elementExpandable.scrollHeight;
        if (elementScrollHeight <= 350) {
            delete elementExpandable.dataset.expandable;
            elementHandle.hidden = true;
        }

        elementHandle.addEventListener('click', function (event) {
            elementExpandable.style.maxHeight = elementExpandable.scrollHeight + 'px';
            elementHandle.hidden = true;
        });
    }
}

initExpandable();
document.addEventListener('turbo:render', initExpandable);

// Autoresize text editors (i.e. textarea)
function initTextEditors() {
    const elementsEditors = document.querySelectorAll('[data-text-editor]');
    for (const elementEditor of elementsEditors) {
        elementEditor.addEventListener('input', function (event) {
            elementEditor.style.height = 'auto';
            elementEditor.style.height = elementEditor.scrollHeight + 'px';
        });
    }
}

initTextEditors();
document.addEventListener('turbo:render', initTextEditors);
