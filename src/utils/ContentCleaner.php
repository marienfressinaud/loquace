<?php

namespace App\utils;

class ContentCleaner
{
    private $dom_sanitizer;

    public function __construct()
    {
        $this->dom_sanitizer = new \SpiderBits\DomSanitizer();
    }

    public function clean(string $content, string $base_url): string
    {
        $content_dom = $this->dom_sanitizer->sanitize($content);

        $content_links = $content_dom->select('//a');
        if ($content_links) {
            foreach ($content_links->list() as $node) {
                if ($node->hasAttribute('href')) {
                    $href = $node->getAttributeNode('href');
                    $url = \SpiderBits\Url::absolutize($href->value, $base_url);
                    $href->value = \Minz\Output\ViewHelpers::protect($url);
                }
            }
        }

        $content_images = $content_dom->select('//img');
        if ($content_images) {
            foreach ($content_images->list() as $node) {
                if ($node->hasAttribute('src')) {
                    $src = $node->getAttributeNode('src');
                    $url = \SpiderBits\Url::absolutize($src->value, $base_url);
                    $src->value = \Minz\Output\ViewHelpers::protect($url);
                }
            }
        }

        return $content_dom->html();
    }
}
