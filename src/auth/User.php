<?php

namespace App\auth;

use App\models;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class User
{
    private static ?models\User $instance = null;

    public static function get(): ?models\User
    {
        if (!self::sessionTokenExists()) {
            return null;
        }

        if (self::$instance !== null) {
            return self::$instance;
        }

        $user = models\User::findBySessionTokenId($_SESSION['session_token_id']);

        if (!$user) {
            unset($_SESSION['session_token_id']);
            return null;
        }

        self::$instance = $user;
        return self::$instance;
    }

    public static function setSessionToken(models\Token $token): void
    {
        self::reset();

        if ($token->hasExpired()) {
            \Minz\Log::warning('Trying to setSessionToken an expired token.');
            return;
        }

        $_SESSION['session_token_id'] = $token->id;
    }

    public static function setSessionTokenId(string $token_id): void
    {
        $token = models\Token::find($token_id);
        if ($token) {
            self::setSessionToken($token);
        }
    }

    public static function sessionTokenId(): ?string
    {
        return $_SESSION['session_token_id'] ?? null;
    }

    public static function sessionTokenExists(): bool
    {
        return isset($_SESSION['session_token_id']);
    }

    public static function reset()
    {
        unset($_SESSION['session_token_id']);
        self::$instance = null;
    }
}
