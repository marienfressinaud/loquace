<?php

namespace App\models;

use Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'feeds_follows')]
class FeedFollow
{
    use Database\Record;
    use Database\Lockable;

    #[Database\Column]
    public string $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public int $user_id;

    #[Database\Column]
    public string $feed_id;

    public ?Feed $feed = null;

    public static function init(User $user, Feed $feed): FeedFollow
    {
        $feed_follow = new self();
        $feed_follow->user_id = $user->id;
        $feed_follow->feed_id = $feed->id;
        return $feed_follow;
    }

    public function feed(): Feed
    {
        if ($this->feed === null) {
            $this->feed = Feed::find($this->feed_id);
        }
        return $this->feed;
    }
}
