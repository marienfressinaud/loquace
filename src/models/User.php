<?php

namespace App\models;

use App\utils;
use Minz\Database;
use Minz\Validable;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'users')]
class User
{
    use Database\Record;
    use Validable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    #[Validable\Presence(message: 'Saisissez un pseudonyme.')]
    #[Validable\Length(max: 42, message: 'Saississez un pseudonyme de moins de {max} caractères.')]
    #[Validable\Unique(message: 'Ce pseudonyme est déjà utilisé, choisissez-en un différent.')]
    #[Validable\Format(pattern: '/^\w[\w\d\.\-\_]*$/', message: 'Saisissez un pseudonyme ne contenant que des lettres ou des chiffres.')]
    public string $handlename;

    #[Database\Column]
    #[Validable\Presence(message: 'Saisissez un mot de passe.')]
    public string $password_hash;

    #[Database\Column]
    public string $avatar_filename = '';

    #[Database\Column]
    public string $personal_feed_id = '';

    public static function init(string $handlename, string $password): User
    {
        $person = new self();
        $person->handlename = $handlename;
        $person->password_hash = self::hashPassword($password);
        return $person;
    }

    public function tagUri(): string
    {
        $host = \Minz\Configuration::$url_options['host'];
        $date = $this->created_at->format('Y-m-d');
        return "tag:{$host},{$date}:user/{$this->id}";
    }

    public static function hashPassword(string $password): string
    {
        if (empty($password)) {
            return '';
        }

        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function verifyPassword(string $password): bool
    {
        return password_verify($password, $this->password_hash);
    }

    public static function findBySessionTokenId($session_token_id): ?User
    {
        $sql = <<<SQL
            SELECT u.* FROM users u, tokens t, sessions s

            WHERE u.id = s.user_id
            AND t.id = s.token_id
            AND t.id = ?
            AND t.expired_at > ?
        SQL;

        $now = \Minz\Time::now()->format(Database\Column::DATETIME_FORMAT);

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([$session_token_id, $now]);
        $result = $statement->fetch();
        if ($result) {
            return Database\Helper::dbToModel(self::class, $result);
        } else {
            return null;
        }
    }

    public function isFollowing(Feed $feed): bool
    {
        return FeedFollow::existsBy([
            'user_id' => $this->id,
            'feed_id' => $feed->id,
        ]);
    }

    public function entries(int $max_number): array
    {
        return Entry::listForUser($this, $max_number);
    }

    public function personalFeed(): Feed
    {
        return Feed::find($this->personal_feed_id);
    }
}
