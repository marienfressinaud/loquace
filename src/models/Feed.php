<?php

namespace App\models;

use Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'feeds')]
class Feed
{
    use Database\Record;
    use Database\Lockable;

    #[Database\Column]
    public string $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public string $url;

    #[Database\Column]
    public string $type = 'unknown';

    #[Database\Column]
    public string $name;

    #[Database\Column]
    public string $description = '';

    #[Database\Column]
    public array $links = [];

    #[Database\Column]
    public string $image_filename = '';

    #[Database\Column]
    public ?\DateTime $image_fetched_at = null;

    #[Database\Column]
    public string $hub_callback_key = '';

    #[Database\Column]
    public string $hub_sub_state = '';

    #[Database\Column]
    public ?\DateTime $hub_sub_expired_at = null;

    #[Database\Column]
    public ?\DateTime $hub_last_ping_at = null;

    #[Database\Column]
    public string $hub_error = '';

    #[Database\Column]
    public string $last_hash = '';

    #[Database\Column]
    public ?\DateTime $locked_at = null;

    #[Database\Column]
    public ?\DateTime $fetched_at = null;

    #[Database\Column]
    public int $fetched_code = 0;

    #[Database\Column]
    public string $fetched_error = '';

    public static function init(string $url): Feed
    {
        $feed = new self();
        $feed->id = \Minz\Random::timebased();
        $feed->url = $url;
        $feed->name = $url;
        return $feed;
    }

    public function entries(int $max_number): array
    {
        return Entry::listForFeed($this, $max_number);
    }

    public function hubSubscriptionAvailable(): bool
    {
        if ($this->hub_sub_state === 'denied') {
            return false;
        }

        if (!$this->hub_sub_expired_at) {
            return true;
        }

        return $this->hub_sub_expired_at <= \Minz\Time::now();
    }

    public function hubEnabled(): bool
    {
        if ($this->hub_sub_state !== 'subscribe') {
            return false;
        }

        if (!$this->hub_sub_expired_at) {
            return false;
        }

        return $this->hub_sub_expired_at > \Minz\Time::now();
    }

    public function shouldBeFetched(): bool
    {
        return (
            !$this->hubEnabled() ||
            $this->hub_last_ping_at < \Minz\Time::ago(1, 'day')
        );
    }

    public function linkAlternate(): string
    {
        return $this->links['alternate'] ?? '';
    }

    public function host(): string
    {
        $url = $this->linkAlternate();

        if (!$url) {
            return '';
        }

        $parsed_url = parse_url($url);
        if (!isset($parsed_url['host'])) {
            return '';
        }

        $host = idn_to_utf8($parsed_url['host'], IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
        if (str_starts_with($host, 'www.')) {
            return substr($host, 4);
        } else {
            return $host;
        }
    }
}
