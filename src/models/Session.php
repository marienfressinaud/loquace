<?php

namespace App\models;

use Minz\Database;

#[Database\Table(name: 'sessions')]
class Session
{
    use Database\Record;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public int $user_id;

    #[Database\Column]
    public string $token_id;

    public static function init(User $user, Token $token): Session
    {
        $session = new self();
        $session->user_id = $user->id;
        $session->token_id = $token->id;
        return $session;
    }
}
