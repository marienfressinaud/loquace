<?php

namespace App\models;

use Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'entries')]
class Entry
{
    use Database\Record;
    use Database\Lockable;
    use BulkQueries;

    #[Database\Column]
    public string $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public string $url;

    #[Database\Column]
    public string $url_hash;

    #[Database\Column]
    public string $title;

    #[Database\Column]
    public string $content = '';

    #[Database\Column]
    public \DateTime $published_at;

    #[Database\Column]
    public int $reading_time = 0;

    #[Database\Column]
    public array $links = [];

    #[Database\Column]
    public string $image_filename = '';

    #[Database\Column]
    public int $feed_id;

    #[Database\Column]
    public string $feed_entry_id = '';

    #[Database\Column]
    public ?\DateTime $locked_at = null;

    #[Database\Column]
    public ?\DateTime $fetched_at = null;

    #[Database\Column]
    public int $fetched_code = 0;

    #[Database\Column]
    public string $fetched_error = '';

    #[Database\Column]
    public int $fetched_count = 0;

    #[Database\Column(computed: true)]
    public int $votes_count = 0;

    public ?Feed $feed = null;

    public static function init(string $url, Feed $feed): Entry
    {
        $entry = new self();
        $entry->id = \Minz\Random::timebased();
        $entry->url = $url;
        $entry->url_hash = sha1($url);
        $entry->title = $url;
        $entry->published_at = \Minz\Time::now();
        $entry->feed_id = $feed->id;
        return $entry;
    }

    public function url(): string
    {
        if ($this->url) {
            return $this->url;
        } else {
            return \Minz\Url::absoluteFor('entry', ['id' => $this->id]);
        }
    }

    public function shortContent(): string
    {
        $dom = \SpiderBits\Dom::fromText($this->content);
        $text = $dom->text();
        $words = explode(' ', $text);
        $short = implode(' ', array_slice($words, 0, 10));
        $short = \mb_strimwidth($short, 0, 100);
        $short = trim($short);
        if (\mb_strlen($text) > \mb_strlen($short)) {
            $short = $short . '…';
        }
        return $short;
    }

    public function tagUri(): string
    {
        $host = \Minz\Configuration::$url_options['host'];
        $date = $this->created_at->format('Y-m-d');
        return "tag:{$host},{$date}:entry/{$this->id}";
    }

    public function feed(): Feed
    {
        if ($this->feed === null) {
            $this->feed = Feed::find($this->feed_id);
        }
        return $this->feed;
    }

    public function replies(): array
    {
        $url = $this->url();

        $sql = <<<SQL
            SELECT e.* FROM entries e
            WHERE e.links ->> 'related' = :url
            ORDER BY e.id ASC
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':url' => $url,
        ]);
        return Database\Helper::dbToModels(self::class, $statement->fetchAll());
    }

    public function countReplies(): int
    {
        $url = $this->url();

        $sql = <<<SQL
            SELECT COUNT(e.*) FROM entries e
            WHERE e.links ->> 'related' = :url
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':url' => $url,
        ]);
        return intval($statement->fetchColumn());
    }

    public function inReplyTo(): string
    {
        return $this->links['related'] ?? '';
    }

    public static function listForFeed(Feed $feed, int $max_number): array
    {
        $sql = <<<SQL
            SELECT e.* FROM entries e
            WHERE e.feed_id = :feed_id
            ORDER BY e.id DESC
            LIMIT :max_number
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':feed_id' => $feed->id,
            ':max_number' => $max_number,
        ]);
        return Database\Helper::dbToModels(self::class, $statement->fetchAll());
    }

    public static function listForUser(User $user, int $max_number): array
    {
        $sql = <<<SQL
            SELECT e.* FROM entries e, feeds_follows ff
            WHERE ff.feed_id = e.feed_id
            AND ff.user_id = :user_id
            ORDER BY e.id DESC
            LIMIT :max_number
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':user_id' => $user->id,
            ':max_number' => $max_number,
        ]);
        return Database\Helper::dbToModels(self::class, $statement->fetchAll());
    }

    public static function getUrlsMapping(Feed $feed): array
    {
        $sql = <<<SQL
            SELECT e.url, e.id FROM entries e
            WHERE e.feed_id = :feed_id
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':feed_id' => $feed->id,
        ]);

        return $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    public static function getFeedEntryIdsMapping(Feed $feed): array
    {
        $sql = <<<SQL
            SELECT e.feed_entry_id, e.id, e.url
            FROM entries e
            WHERE e.feed_id = :feed_id
            ORDER BY e.published_at DESC, e.id
            LIMIT 1000
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':feed_id' => $feed->id,
        ]);

        return $statement->fetchAll(\PDO::FETCH_UNIQUE);
    }

    public static function validateUrl(string $url): bool
    {
        $validate = filter_var($url, FILTER_VALIDATE_URL) !== false;
        if (!$validate) {
            return false;
        }

        $parsed_url = parse_url($url);
        if ($parsed_url['scheme'] !== 'http' && $parsed_url['scheme'] !== 'https') {
            return false;
        }

        if (isset($parsed_url['pass'])) {
            return false;
        }

        return true;
    }
}
