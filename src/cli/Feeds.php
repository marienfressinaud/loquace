<?php

namespace App\cli;

use App\models;
use App\services;
use Minz\Request;
use Minz\Response;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Feeds
{
    public function index(Request $request): Response
    {
        $feeds = models\Feed::listAll();
        $feeds_as_text = [];
        foreach ($feeds as $feed) {
            if (!$feed->url) {
                continue;
            }

            $feed_as_text = "{$feed->id} {$feed->url}";
            if ($feed->isLocked()) {
                $feed_as_text .= ' (locked)';
            }

            $feeds_as_text[] = $feed_as_text;
        }

        if (!$feeds_as_text) {
            $feeds_as_text[] = 'No feeds to list.';
        }

        return Response::text(200, implode("\n", $feeds_as_text));
    }

    public function sync(Request $request): Response
    {
        $id = $request->param('id');
        $nocache = $request->paramBoolean('nocache', false);
        $feed = models\Feed::find($id);
        if (!$feed || !$feed->url) {
            return Response::text(404, "Feed id `{$id}` does not exist.");
        }

        $feed_fetcher_service = new services\FeedFetcher([
            'cache' => !$nocache,
        ]);
        $feed_fetcher_service->fetch($feed);

        return Response::text(200, "Feed {$id} ({$feed->url}) has been synchronized.");
    }

    public function unlock(Request $request): Response
    {
        $id = $request->param('id');
        $feed = models\Feed::find($id);
        if (!$feed) {
            return Response::text(404, "Feed id `{$id}` does not exist.");
        }

        if (!$feed->isLocked()) {
            return Response::text(200, "Feed {$feed->id} ({$feed->url}) was not locked.");
        }

        $feed->unlock();

        return Response::text(200, "Feed {$feed->id} ({$feed->url}) has been unlocked.");
    }
}
