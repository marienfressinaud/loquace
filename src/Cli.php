<?php

namespace App;

class Cli
{
    private $engine;

    public function __construct()
    {
        $router = new Router();
        $this->engine = new \Minz\Engine($router);
        \Minz\Url::setRouter($router);
    }

    public function run($request)
    {
        $bin = $request->param('bin');
        $bin = $bin === 'cli' ? 'php cli' : $bin;

        $current_command = $request->path();
        $current_command = trim(str_replace('/', ' ', $current_command));

        \Minz\Output\View::declareDefaultVariables([
            'bin' => $bin,
            'current_command' => $current_command,
        ]);

        return $this->engine->run($request, [
            'not_found_view_pointer' => 'cli/not_found.txt',
            'internal_server_error_view_pointer' => 'cli/internal_server_error.txt',
            'controller_namespace' => '\\App\\cli',
        ]);
    }
}
