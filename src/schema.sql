CREATE TABLE feeds (
    id TEXT PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL,
    url TEXT NOT NULL,
    type TEXT NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL DEFAULT '',
    links JSON NOT NULL DEFAULT '{}',
    image_filename TEXT NOT NULL DEFAULT '',
    image_fetched_at TIMESTAMPTZ,

    hub_callback_key TEXT NOT NULL DEFAULT '',
    hub_sub_state TEXT NOT NULL DEFAULT '',
    hub_sub_expired_at TIMESTAMPTZ,
    hub_last_ping_at TIMESTAMPTZ,
    hub_error TEXT NOT NULL DEFAULT '',

    last_hash TEXT NOT NULL DEFAULT '',
    locked_at TIMESTAMPTZ,
    fetched_at TIMESTAMPTZ,
    fetched_code INTEGER NOT NULL DEFAULT 0,
    fetched_error TEXT NOT NULL DEFAULT ''
);

CREATE INDEX idx_feeds_fetched_at ON feeds(fetched_at);

CREATE TABLE entries (
    id TEXT PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL,

    url TEXT NOT NULL,
    url_hash TEXT NOT NULL,
    title TEXT NOT NULL,
    content TEXT NOT NULL DEFAULT '',
    published_at TIMESTAMPTZ NOT NULL,
    reading_time INTEGER NOT NULL DEFAULT 0,
    links JSON NOT NULL DEFAULT '{}',
    image_filename TEXT NOT NULL DEFAULT '',

    feed_id TEXT REFERENCES feeds ON DELETE CASCADE ON UPDATE CASCADE,
    feed_entry_id TEXT NOT NULL DEFAULT '',

    locked_at TIMESTAMPTZ,
    fetched_at TIMESTAMPTZ,
    fetched_code INTEGER NOT NULL DEFAULT 0,
    fetched_error TEXT NOT NULL DEFAULT '',
    fetched_count INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX idx_entries_url ON entries(url_hash);
CREATE INDEX idx_entries_published_at ON entries(published_at);
CREATE INDEX idx_entries_fetched_at ON entries(fetched_at) WHERE fetched_at IS NULL;
CREATE INDEX idx_entries_fetched_code ON entries(fetched_code) WHERE fetched_code < 200 OR fetched_code >= 300;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL,

    handlename TEXT UNIQUE NOT NULL,
    password_hash TEXT NOT NULL,
    avatar_filename TEXT,

    personal_feed_id TEXT UNIQUE REFERENCES feeds ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE tokens (
    id TEXT PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL,
    expired_at TIMESTAMPTZ NOT NULL
);

CREATE TABLE sessions (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL,
    user_id INTEGER REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE,
    token_id TEXT UNIQUE REFERENCES tokens ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE INDEX idx_sessions_token ON sessions(token_id);

CREATE TABLE feeds_follows (
    id BIGSERIAL PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL,
    user_id INTEGER REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE,
    feed_id TEXT REFERENCES feeds ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE UNIQUE INDEX idx_feeds_follows ON feeds_follows(user_id, feed_id);

CREATE TABLE jobs (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL,
    perform_at TIMESTAMPTZ NOT NULL,
    name TEXT NOT NULL DEFAULT '',
    handler JSON NOT NULL DEFAULT '{}',
    frequency TEXT NOT NULL DEFAULT '',
    queue TEXT NOT NULL DEFAULT 'default',
    locked_at TIMESTAMPTZ,
    number_attempts BIGINT NOT NULL DEFAULT 0,
    last_error TEXT NOT NULL DEFAULT '',
    failed_at TIMESTAMPTZ
);
