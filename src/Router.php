<?php

namespace App;

class Router extends \Minz\Router
{
    public function __construct()
    {
        parent::__construct();

        $this->addRoute('get', '/', 'Timeline#show', 'home');

        $this->addRoute('get', '/registration', 'Users#new', 'registration');
        $this->addRoute('post', '/registration', 'Users#create', 'register');
        $this->addRoute('get', '/login', 'Sessions#new', 'new login');
        $this->addRoute('post', '/login', 'Sessions#create', 'login');
        $this->addRoute('post', '/logout', 'Sessions#delete', 'logout');

        $this->addRoute('get', '/u/:handlename', 'Users#show', 'user');
        $this->addRoute('get', '/u/:handlename/follows', 'users/Follows#index', 'user follows');
        $this->addRoute('get', '/u/:handlename/feed', 'Users#atom', 'user atom');

        $this->addRoute('get', '/f/search', 'feeds/Searches#new', 'new search feed');
        $this->addRoute('post', '/f/search', 'feeds/Searches#create', 'search feed');
        $this->addRoute('get', '/f/:id', 'Feeds#show', 'feed');
        $this->addRoute('post', '/f/:id/follow', 'feeds/Follows#create', 'follow feed');
        $this->addRoute('post', '/f/:id/unfollow', 'feeds/Follows#delete', 'unfollow feed');

        $this->addRoute('post', '/entry', 'Entries#create', 'create entry');
        $this->addRoute('get', '/e/:id', 'Entries#show', 'entry');

        $this->addRoute('get', '/hub/:key', 'Hub#subscription', 'hub subscription');
        $this->addRoute('post', '/hub/:key', 'Hub#delivery', 'hub delivery');

        $this->addRoute('cli', '/system/init', 'System#init');
        $this->addRoute('cli', '/system/update', 'System#update');

        $this->addRoute('cli', '/feeds', 'Feeds#index');
        $this->addRoute('cli', '/feeds/sync', 'Feeds#sync');
        $this->addRoute('cli', '/feeds/unlock', 'Feeds#unlock');

        $this->addRoute('cli', '/jobs', 'Jobs#index');
        $this->addRoute('cli', '/jobs/install', 'Jobs#install');
        $this->addRoute('cli', '/jobs/run', 'Jobs#run');
        $this->addRoute('cli', '/jobs/unlock', 'Jobs#unlock');
        $this->addRoute('cli', '/jobs/watch', 'Jobs#watch');
    }

    public static function pathIsRedirectable(string $path): bool
    {
        $router = new self();
        try {
            $router->match('get', $path);
            return true;
        } catch (\Minz\Errors\RouteNotFoundError $e) {
            return false;
        }
    }
}
