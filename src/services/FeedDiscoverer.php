<?php

namespace App\services;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class FeedDiscoverer
{
    /** @var \SpiderBits\Cache */
    private $cache;

    /** @var \SpiderBits\Http */
    private $http;

    public function __construct()
    {
        $cache_path = \Minz\Configuration::$application['cache_path'];
        $this->cache = new \SpiderBits\Cache($cache_path);

        $this->http = new \SpiderBits\Http();
        $this->http->user_agent = \Minz\Configuration::$application['user_agent'];
        $this->http->timeout = 10;
    }

    public function search(string $url): array
    {
        $url_hash = \SpiderBits\Cache::hash($url);
        $cached_response = $this->cache->get($url_hash);
        if ($cached_response) {
            $response = \SpiderBits\Response::fromText($cached_response);
        } else {
            try {
                $response = $this->http->get($url, [], [
                    'max_size' => 20 * 1024 * 1024,
                ]);
            } catch (\SpiderBits\HttpError $e) {
                return [];
            }

            if ($response->success) {
                $this->cache->save($url_hash, (string)$response);
            }
        }

        if (!$response->success) {
            return [];
        }

        $encodings = mb_list_encodings();
        $data = mb_convert_encoding($response->data, 'UTF-8', $encodings);

        $content_type = $response->header('content-type');
        if (
            \SpiderBits\feeds\Feed::isFeedContentType($content_type) &&
            \SpiderBits\feeds\Feed::isFeed($data)
        ) {
            return [$url];
        }

        $urls = [];

        $dom = \SpiderBits\Dom::fromText($data);
        $feeds_urls = \SpiderBits\DomExtractor::feeds($dom);
        foreach ($feeds_urls as $feed_url) {
            $feed_url = \SpiderBits\Url::absolutize($feed_url, $url);
            $feed_url = \SpiderBits\Url::sanitize($feed_url);
            $urls[] = $feed_url;
        }

        return $urls;
    }
}
