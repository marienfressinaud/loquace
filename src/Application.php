<?php

namespace App;

class Application
{
    private \Minz\Router $router;

    private \Minz\Engine $engine;

    public function __construct()
    {
        include_once('utils/view_helpers.php');

        $this->router = new Router();
        $this->engine = new \Minz\Engine($this->router);
        \Minz\Url::setRouter($this->router);

        \Minz\Output\View::$extensions_to_content_types['.atom.xml.phtml'] = 'application/xml';
    }

    public function run($request)
    {
        $session_token_id = $request->cookie('session_token');
        if (auth\User::sessionTokenExists() && !$session_token_id) {
            auth\User::reset();
        } elseif (!auth\User::sessionTokenExists() && $session_token_id) {
            auth\User::setSessionTokenId($session_token_id);
        }

        $current_user = auth\User::get();

        $response = $this->engine->run($request, [
            'not_found_view_pointer' => 'not_found.phtml',
            'internal_server_error_view_pointer' => 'internal_server_error.phtml',
            'controller_namespace' => '\\App\\controllers',
        ]);

        \Minz\Output\View::declareDefaultVariables([
            'environment' => \Minz\Configuration::$environment,
            'brand' => \Minz\Configuration::$application['brand'],
            'user_agent' => \Minz\Configuration::$application['user_agent'],
            'alternates' => [],
            'csrf' => \Minz\CSRF::generate(),
            'current_user' => $current_user,
            'current_page' => '',
            'query' => $request->param('q', ''),
            'now' => \Minz\Time::now(),
        ]);

        $response->setContentSecurityPolicy('img-src', '*');

        return $response;
    }
}
