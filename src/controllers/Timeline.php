<?php

namespace App\controllers;

use App\auth;
use App\models;
use App\utils;
use \Minz\Request;
use \Minz\Response;

class Timeline
{
    public function show(Request $request): Response
    {
        $user = auth\User::get();

        if (!$user) {
            return Response::ok('pages/home.phtml');
        }

        return Response::ok('timeline/show.phtml', [
            'content' => '',
        ]);
    }
}
