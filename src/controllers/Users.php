<?php

namespace App\controllers;

use App\auth;
use App\models;
use \Minz\Request;
use \Minz\Response;

class Users
{
    public function show(Request $request): Response
    {
        $handlename = $request->param('handlename', '');
        $user = models\User::findBy(['handlename' => $handlename]);

        return \Minz\Response::ok('users/show.phtml', [
            'user' => $user,
            'feed' => $user->personalFeed(),
            'content' => '',
        ]);
    }

    public function atom(Request $request): Response
    {
        $handlename = $request->param('handlename', '');
        $user = models\User::findBy(['handlename' => $handlename]);

        return \Minz\Response::ok('users/show.atom.xml.phtml', [
            'user' => $user,
            'feed' => $user->personalFeed(),
            'hub' => \Minz\Configuration::$application['websub_hub'],
        ]);
    }

    public function new(Request $request): Response
    {
        if (auth\User::get()) {
            return \Minz\Response::redirect('home');
        }

        return \Minz\Response::ok('users/new.phtml', [
            'handlename' => '',
            'password' => '',
        ]);
    }

    public function create(Request $request): Response
    {
        if (auth\User::get()) {
            return \Minz\Response::redirect('home');
        }

        $csrf = $request->param('csrf', '');

        $handlename = trim($request->param('handlename', ''));

        $password = $request->param('password', '');

        if (!\Minz\CSRF::validate($csrf)) {
            return \Minz\Response::badRequest('users/new.phtml', [
                'handlename' => $handlename,
                'password' => $password,
                'errors' => [
                    'csrf' => 'Un problème est survenu, veuillez valider le formulaire à nouveau.',
                ],
            ]);
        }

        $user = models\User::init($handlename, $password);

        $errors = $user->validate();
        if ($errors) {
            return \Minz\Response::badRequest('users/new.phtml', [
                'handlename' => $handlename,
                'password' => $password,
                'errors' => $errors,
            ]);
        }

        $feed = models\Feed::init('');
        $feed->name = "@{$handlename}";
        $feed->save();

        $user->personal_feed_id = $feed->id;
        $user->save();

        $feed_follow = models\FeedFollow::init($user, $feed);
        $feed_follow->save();

        $token = models\Token::init(1, 'month');
        $session = models\Session::init($user, $token);
        $token->save();
        $session->save();

        auth\User::setSessionToken($token);

        $response = \Minz\Response::redirect('home');
        $response->setCookie('session_token', $token->id, [
            'expires' => $token->expired_at->getTimestamp(),
            'samesite' => 'Lax',
        ]);
        return $response;
    }
}
