<?php

namespace App\controllers\feeds;

use App\auth;
use App\models;
use App\services;
use \Minz\Request;
use \Minz\Response;

class Follows
{
    public function create(Request $request): Response
    {
        $feed_id = $request->param('id', '');
        $feed = models\Feed::find($feed_id);

        $from = $request->param('from');
        if (!\App\Router::pathIsRedirectable($from)) {
            return Response::redirect('home');
        }

        $user = auth\User::get();
        if (!$user) {
            return Response::found($from);
        }

        if ($user->isFollowing($feed)) {
            return Response::found($from);
        }

        $feed_follow = models\FeedFollow::init($user, $feed);
        $feed_follow->save();

        return Response::found($from);
    }

    public function delete(Request $request): Response
    {
        $feed_id = $request->param('id', '');
        $feed = models\Feed::find($feed_id);

        $from = $request->param('from');
        if (!\App\Router::pathIsRedirectable($from)) {
            return Response::redirect('home');
        }

        $user = auth\User::get();
        if (!$user) {
            return Response::found($from);
        }

        if (!$user->isFollowing($feed)) {
            return Response::found($from);
        }

        models\FeedFollow::deleteBy([
            'user_id' => $user->id,
            'feed_id' => $feed->id,
        ]);

        return Response::found($from);
    }
}
