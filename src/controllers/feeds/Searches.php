<?php

namespace App\controllers\feeds;

use App\auth;
use App\models;
use App\services;
use \Minz\Request;
use \Minz\Response;

class Searches
{
    public function new(Request $request): Response
    {
        return Response::ok('feeds/searches/new.phtml');
    }

    public function create(Request $request): Response
    {
        $user = auth\User::get();

        if (!$user) {
            return Response::redirect('login');
        }

        $url = $request->param('q', '');

        $csrf = $request->param('csrf');

        if (!\Minz\CSRF::validate($csrf)) {
            return Response::badRequest('feeds/searches/new.phtml', [
                'url' => $url,
                'errors' => [
                    'csrf' => 'Un problème est survenu, veuillez valider le formulaire à nouveau.',
                ],
            ]);
        }

        $url = \SpiderBits\Url::sanitize($url);

        $feed_discoverer = new services\FeedDiscoverer();
        $feeds_urls = $feed_discoverer->search($url);

        if (count($feeds_urls) >= 1) {
            $url = $feeds_urls[0];
            $feed = models\Feed::findBy(['url' => $url]);

            if (!$feed) {
                $feed_fetcher_service = new services\FeedFetcher(['timeout' => 10]);
                $feed = models\Feed::init($url);
                $feed_fetcher_service->fetch($feed);
            }

            return Response::redirect('feed', ['id' => $feed->id]);
        } else {
            return Response::redirect('new search feed', [
                'q' => $url,
            ]);
        }
    }
}
