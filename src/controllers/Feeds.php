<?php

namespace App\controllers;

use App\models;
use \Minz\Request;
use \Minz\Response;

class Feeds
{
    public function show(Request $request): Response
    {
        $feed_id = $request->paramInteger('id');

        $feed = models\Feed::find($feed_id);

        if ($feed->url) {
            return Response::ok('feeds/show.phtml', [
                'feed' => $feed,
            ]);
        } else {
            $user = models\User::findBy(['personal_feed_id' => $feed->id]);
            return Response::redirect('user', ['handlename' => $user->handlename]);
        }
    }
}
