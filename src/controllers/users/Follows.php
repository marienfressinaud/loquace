<?php

namespace App\controllers\users;

use App\auth;
use App\models;
use \Minz\Request;
use \Minz\Response;

class Follows
{
    public function index(Request $request): Response
    {
        $handlename = $request->param('handlename', '');
        $user = models\User::findBy(['handlename' => $handlename]);

        $follows = models\FeedFollow::listBy([
            'user_id' => $user->id,
        ]);

        $collator = new \Collator('fr_FR');
        usort($follows, function ($follow1, $follow2) use ($collator) {
            $feed_name1 = $follow1->feed()->name;
            $feed_name2 = $follow2->feed()->name;
            return $collator->compare($feed_name1, $feed_name2);
        });

        return \Minz\Response::ok('users/follows/index.phtml', [
            'user' => $user,
            'feed' => $user->personalFeed(),
            'follows' => $follows,
        ]);
    }
}
