<?php

namespace App\controllers;

use App\models;
use App\services;
use Minz\Request;
use Minz\Response;

class Hub
{
    public function subscription(Request $request): Response
    {
        $key = $request->param('key', '');

        $mode = $request->param('hub_mode', '');
        $topic = $request->param('hub_topic', '');
        $challenge = $request->param('hub_challenge', '');
        $lease_seconds = $request->paramInteger('hub_lease_seconds', 24 * 60 * 60); // 1 day
        $reason = $request->param('hub_reason', '');

        $feed = models\Feed::findBy(['hub_callback_key' => $key]);
        if (!$feed) {
            return Response::notFound();
        }

        if ($feed->links['self'] !== $topic) {
            return Response::notFound();
        }

        $feed->hub_last_ping_at = \Minz\Time::now();

        if ($mode === 'denied') {
            $feed->hub_sub_state = 'denied';
            $feed->hub_error = $request->param('@input', '');

            return Response::ok();
        } elseif ($mode === 'subscribe' && $feed->hub_sub_state === 'pending subscription') {
            $feed->hub_sub_state = 'subscribe';
            $feed->hub_sub_expired_at = \Minz\Time::fromNow($lease_seconds, 'seconds');
            $feed->hub_error = '';

            return Response::text(200, $challenge);
        } elseif ($mode === 'unsubscribe' && $feed->hub_sub_state === 'pending unsubscription') {
            $feed->hub_sub_state = 'unsubscribe';
            $feed->hub_error = '';

            return Response::text(200, $challenge);
        } else {
            return Response::notFound();
        }
    }

    public function delivery(Request $request): Response
    {
        $key = $request->param('key', '');

        $feed = models\Feed::findBy(['hub_callback_key' => $key]);
        if (!$feed) {
            return new Response(410);
        }

        $feed->hub_last_ping_at = \Minz\Time::now();

        // Cache the content so the FeedFetcher doesn't do an HTTP request
        $feed_new_content = $request->param('@input', '');

        $content_type = $request->header('CONTENT_TYPE', '');

        if (!$content_type) {
            $feed->save();
            return new Response(400);
        }

        $raw_response = <<<TEXT
            HTTP/2 200 OK
            Content-Type: {$content_type}

            {$feed_new_content}
            TEXT;

        $hash = \SpiderBits\Cache::hash($feed->url);
        $cache_path = \Minz\Configuration::$application['cache_path'];
        $cache = new \SpiderBits\Cache($cache_path);
        $cache->save($hash, $raw_response);

        // Then, fetch and synchronize the feed
        $feed_fetcher_service = new services\FeedFetcher();
        $feed_fetcher_service->fetch($feed);

        return Response::ok();
    }
}
