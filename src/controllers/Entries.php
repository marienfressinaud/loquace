<?php

namespace App\controllers;

use App\auth;
use App\models;
use App\utils;
use \Minz\Request;
use \Minz\Response;

class Entries
{
    public function show(Request $request): Response
    {
        $entry_id = $request->param('id', '');
        $entry = models\Entry::find($entry_id);

        return \Minz\Response::ok('entries/show.phtml', [
            'entry' => $entry,
            'content' => '',
        ]);
    }

    public function create(Request $request): Response
    {
        $user = auth\User::get();

        if (!$user) {
            return Response::redirect('login');
        }

        $md_content = $request->param('content', '');

        $reply_to = $request->param('reply_to', '');

        $csrf = $request->param('csrf');

        $from = $request->param('from');
        if (!\App\Router::pathIsRedirectable($from)) {
            return Response::redirect('home');
        }

        if (!\Minz\CSRF::validate($csrf)) {
            return Response::found($from);
        }

        $parsedown = new \Parsedown();
        $content = $parsedown->text($md_content);

        $dom_sanitizer = new \SpiderBits\DomSanitizer();
        $content_dom = $dom_sanitizer->sanitize($content);

        $feed_user = $user->personalFeed();

        $entry = models\Entry::init('', $feed_user);
        $entry->content = $content_dom->html();

        if ($reply_to) {
            $entry->links['related'] = $reply_to;
        }

        $entry->save();

        // notice the Websub hub that a new entry has been published
        $websub_hub = \Minz\Configuration::$application['websub_hub'];
        if ($websub_hub) {
            $http = new \SpiderBits\Http();
            $http->user_agent = \Minz\Configuration::$application['user_agent'];
            $http->timeout = 5;
            $http->post($websub_hub, [
                'hub.mode' => 'publish',
                'hub.url' => \Minz\Url::absoluteFor('user atom', ['handlename' => $user->handlename]),
            ]);
        }

        return Response::found($from);
    }
}
