<?php

namespace App\controllers;

use App\auth;
use App\models;
use \Minz\Request;
use \Minz\Response;

class Sessions
{
    public function new(Request $request): Response
    {
        if (auth\User::get()) {
            return \Minz\Response::redirect('home');
        }

        return \Minz\Response::ok('sessions/new.phtml', [
            'handlename' => '',
            'password' => '',
        ]);
    }

    public function create(Request $request): Response
    {
        if (auth\User::get()) {
            return \Minz\Response::redirect('home');
        }

        $csrf = $request->param('csrf', '');

        $handlename = trim($request->param('handlename', ''));

        $password = $request->param('password', '');

        if (!\Minz\CSRF::validate($csrf)) {
            return \Minz\Response::badRequest('sessions/new.phtml', [
                'handlename' => $handlename,
                'password' => $password,
                'errors' => [
                    'csrf' => 'Un problème est survenu, veuillez valider le formulaire à nouveau.',
                ],
            ]);
        }

        $user = models\User::findBy(['handlename' => $handlename]);
        if (!$user) {
            return \Minz\Response::badRequest('sessions/new.phtml', [
                'handlename' => $handlename,
                'password' => $password,
                'errors' => [
                    'handlename' => 'Il n’existe aucun compte correspondant à ce pseudonyme.',
                ],
            ]);
        }

        if (!$user->verifyPassword($password)) {
            return \Minz\Response::badRequest('sessions/new.phtml', [
                'handlename' => $handlename,
                'password' => $password,
                'errors' => [
                    'password' => 'Le mot de passe est incorrect, veuillez réessayer.',
                ],
            ]);
        }

        $token = models\Token::init(1, 'month');
        $session = models\Session::init($user, $token);
        $token->save();
        $session->save();

        auth\User::setSessionToken($token);

        $response = \Minz\Response::redirect('home');
        $response->setCookie('session_token', $token->id, [
            'expires' => $token->expired_at->getTimestamp(),
            'samesite' => 'Lax',
        ]);
        return $response;
    }

    public function delete(Request $request): Response
    {
        $user = auth\User::get();
        if (!$user) {
            return \Minz\Response::redirect('home');
        }

        $csrf = $request->param('csrf', '');

        if (!\Minz\CSRF::validate($csrf)) {
            return \Minz\Response::redirect('home');
        }

        $session_token_id = auth\User::sessionTokenId();
        $session = models\Session::findBy(['token_id' => $session_token_id]);
        $session->remove();
        auth\User::reset();

        $response = \Minz\Response::redirect('home');
        $response->removeCookie('session_token');
        return $response;
    }
}
