# Loquace

Loquace est un réseau social basé sur les flux Web pour fonctionner.
Publiez et échangez avec d’autres, tout en suivant vos flux favoris.

## Utilisation

Une instance de Loquace est disponible ici : [loquace.yuzu.ovh](https://loquace.yuzu.ovh).

## Technique

Loquace repose sur

- mon framework maison, [Minz](/lib/Minz) ;
- [SpiderBits](/lib/SpiderBits), une bibliothèque maison pour parser le Web ;
- pas mal de bouts de code issus de [flusio](https://github.com/flusio/flusio).

Il n’existe malheureusement pas de documentation pour installer Loquace.
La documentation de [flusio](https://github.com/flusio/flusio/blob/main/docs/production.md) devrait toutefois constituer une bonne base.

## Prototype

Loquace est un prototype.
C’est-à-dire qu’il se contente de montrer une petite partie de son potentiel en se concentrant sur l’essentiel.

Sur le plan technique, c’était l’occasion :

- d’écrire un nettoyeur simple de HTML en PHP ;
- d’implémenter du temps réel via le protocole [Websub](https://fr.wikipedia.org/wiki/WebSub) ;
- de montrer la possibilité technique d’avoir un système standard de réponses via les flux Web.

Loquace fonctionne, mais il pourrait être être amélioré.
Il n’est toutefois pas prévu de le faire évoluer.

## Licence

Loquace est disponible sous [licence AGPL v3.0 ou plus](/LICENSE.txt).
