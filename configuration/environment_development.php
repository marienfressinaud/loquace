<?php

$db_host = $dotenv->pop('DB_HOST');
$db_port = intval($dotenv->pop('DB_PORT', '5432'));
$db_name = $dotenv->pop('DB_NAME', 'loquace_development');

$user_agent = "loquace/dev (https://framagit.org/marienfressinaud/loquace)";

$feeds_links_keep_maximum = max(0, intval($dotenv->pop('FEEDS_LINKS_KEEP_MAXIMUM', '0')));

return [
    'app_name' => 'App',

    'secret_key' => $dotenv->pop('APP_SECRET_KEY'),

    'url_options' => [
        'host' => $dotenv->pop('APP_HOST'),
        'path' => $dotenv->pop('APP_PATH', '/'),
        'port' => intval($dotenv->pop('APP_PORT', '80')),
    ],

    'data_path' => $dotenv->pop('APP_DATA_PATH', $app_path . '/data'),

    'application' => [
        'brand' => $dotenv->pop('APP_BRAND', 'Loquace'),
        'user_agent' => $user_agent,
        'cache_path' => $dotenv->pop('APP_CACHE_PATH', $app_path . '/cache'),
        'media_path' => $dotenv->pop('APP_MEDIA_PATH', $app_path . '/public/static/media'),
        'registrations_opened' => filter_var($dotenv->pop('APP_OPEN_REGISTRATIONS', true), FILTER_VALIDATE_BOOLEAN),
        'feeds_links_keep_maximum' => $feeds_links_keep_maximum,
        'websub_enabled' => false,
        'websub_hub' => $dotenv->pop('APP_WEBSUB_HUB', ''),
    ],

    'database' => [
        'dsn' => "pgsql:host={$db_host};port={$db_port};dbname={$db_name}",
        'username' => $dotenv->pop('DB_USERNAME'),
        'password' => $dotenv->pop('DB_PASSWORD'),
    ],
];
