<?php

namespace Minz\Jobs;

use Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'jobs')]
class Job
{
    use Database\Record;
    use Database\Lockable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public string $name;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public array $handler;

    #[Database\Column]
    public \DateTime $perform_at;

    #[Database\Column]
    public string $frequency = '';

    #[Database\Column]
    public string $queue = 'default';

    #[Database\Column]
    public ?\DateTime $locked_at = null;

    #[Database\Column]
    public int $number_attempts = 0;

    #[Database\Column]
    public string $last_error = '';

    #[Database\Column]
    public ?\DateTime $failed_at = null;

    public function __construct()
    {
        $this->name = get_called_class();
        $this->perform_at = \Minz\Time::now();
    }

    public function performLater(mixed ...$args): void
    {
        $this->handler = [
            'job_class' => get_called_class(),
            'job_args' => $args,
        ];
        $this->save();
    }

    public static function findNextJob($queue): ?Job
    {
        $values = [
            ':perform_at' => \Minz\Time::now()->format(Database\Column::DATETIME_FORMAT),
            ':lock_timeout' => \Minz\Time::ago(1, 'hour')->format(Database\Column::DATETIME_FORMAT),
        ];

        $queue_placeholder = '';
        if ($queue !== 'all') {
            $queue_placeholder = 'AND queue = :queue';
            $values[':queue'] = $queue;
        }

        $sql = <<<SQL
            SELECT * FROM jobs
            WHERE (locked_at IS NULL OR locked_at <= :lock_timeout)
            AND perform_at <= :perform_at
            AND (number_attempts <= 25 OR frequency != '')
            {$queue_placeholder}
            ORDER BY perform_at, created_at;
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute($values);
        $result = $statement->fetch();
        if ($result !== false) {
            return Database\Helper::dbToModel(self::class, $result);
        } else {
            return null;
        }
    }

    public function reschedule()
    {
        if (!$this->frequency) {
            return;
        }

        $this->last_error = '';
        $this->failed_at = null;
        $this->perform_at = $this->nextPerformAt();
        $this->save();
    }

    private function nextPerformAt()
    {
        $date = clone $this->perform_at;
        while ($date <= \Minz\Time::now()) {
            $date->modify($this->frequency);
        }
        return $date;
    }

    public function fail($error)
    {
        if ($this->frequency) {
            $next_perform_at = $this->nextPerformAt();
        } else {
            $number_seconds = 5 + pow($this->number_attempts, 4);
            $next_perform_at = \Minz\Time::fromNow($number_seconds, 'seconds');
        }

        $this->perform_at = $next_perform_at;
        $this->failed_at = \Minz\Time::now();
        $this->last_error = $error;
        $this->save();
    }
}
