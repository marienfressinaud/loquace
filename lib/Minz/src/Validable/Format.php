<?php

namespace Minz\Validable;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Format extends Check
{
    public string $pattern;

    public function __construct(string $pattern, string $message)
    {
        parent::__construct($message);
        $this->pattern = $pattern;
    }

    public function assert(): bool
    {
        return preg_match($this->pattern, $this->getValue());
    }
}
