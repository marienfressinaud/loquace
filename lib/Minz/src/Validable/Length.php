<?php

namespace Minz\Validable;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Length extends Check
{
    public ?int $min;

    public ?int $max;

    public function __construct(string $message, ?int $min = null, ?int $max = null)
    {
        parent::__construct($message);
        $this->min = $min;
        $this->max = $max;
    }

    public function assert(): bool
    {
        $length = $this->getLength();

        if ($this->min !== null && $length < $this->min) {
            return false;
        }

        if ($this->max !== null && $length > $this->max) {
            return false;
        }

        return true;
    }

    public function getMessage(): string
    {
        $length = $this->getLength();

        return str_replace(
            ['{min}', '{max}', '{length}'],
            [$this->min, $this->max, $length],
            $this->message,
        );
    }

    private function getLength(): int
    {
        $value = strval($this->getValue());
        return mb_strlen($value);
    }
}
