<?php

namespace Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Column
{
    public const DATETIME_FORMAT = 'Y-m-d H:i:sP';

    public bool $computed;

    public function __construct(bool $computed = false)
    {
        $this->computed = $computed;
    }
}
