<?php

namespace Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Table
{
    public string $name;

    public string $primary_key;

    public function __construct(string $name, string $primary_key = 'id')
    {
        $this->name = $name;
        $this->primary_key = $primary_key;
    }
}
