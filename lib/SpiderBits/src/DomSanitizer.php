<?php

namespace SpiderBits;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class DomSanitizer
{
    public const ALLOWED_ELEMENTS = [
        'abbr' => [],
        'a' => ['href', 'title'],
        'blockquote' => [],
        'br' => [],
        'caption' => [],
        'code' => [],
        'dd' => [],
        'del' => [],
        'details' => ['open'],
        'div' => [],
        'dl' => [],
        'dt' => [],
        'em' => [],
        'figcaption' => [],
        'figure' => [],
        'h1' => [],
        'h2' => [],
        'h3' => [],
        'h4' => [],
        'h5' => [],
        'h6' => [],
        'hr' => [],
        'img' => ['src', 'alt', 'title'],
        'i' => [],
        'li' => [],
        'ol' => [],
        'pre' => [],
        'p' => [],
        'q' => [],
        'rp' => [],
        'rt' => [],
        'ruby' => [],
        'small' => [],
        'span' => [],
        'strong' => [],
        'sub' => [],
        'summary' => [],
        'sup' => [],
        'table' => [],
        'tbody' => [],
        'td' => [],
        'tfoot' => [],
        'thead' => [],
        'th' => [],
        'tr' => [],
        'u' => [],
        'ul' => [],
    ];

    /** @var \DOMDocument */
    private $healthy_dom;

    public function sanitize($html_as_string)
    {
        $this->healthy_dom = new \DOMDocument();

        // Make sure to have a root node
        $html_as_string = "<div>{$html_as_string}</div>";

        $dirty_dom = new \DOMDocument();
        @$dirty_dom->loadHTML(
            mb_convert_encoding($html_as_string, 'HTML-ENTITIES', 'UTF-8'),
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );

        $this->sanitizeNode(
            $this->healthy_dom,
            $dirty_dom->documentElement,
        );

        return new Dom($this->healthy_dom);
    }

    private function sanitizeNode($healthy_parent_node, $dirty_node)
    {
        if ($dirty_node instanceof \DOMText) {
            $healthy_node = new \DOMText($dirty_node->nodeValue);
            $healthy_parent_node->appendChild($healthy_node);
        } elseif ($dirty_node instanceof \DOMElement) {
            if (!isset(self::ALLOWED_ELEMENTS[$dirty_node->tagName])) {
                return;
            }

            $healthy_node = $this->healthy_dom->createElement($dirty_node->tagName);

            $allowed_attributes = self::ALLOWED_ELEMENTS[$dirty_node->tagName];
            foreach ($dirty_node->attributes as $dirty_attr_name => $dirty_attr_node) {
                if (!in_array($dirty_attr_name, $allowed_attributes)) {
                    continue;
                }

                $healty_attr_node = new \DOMAttr(
                    $dirty_attr_node->name,
                    $dirty_attr_node->value,
                );

                $healthy_node->appendChild($healty_attr_node);
            }

            $healthy_parent_node->appendChild($healthy_node);

            foreach ($dirty_node->childNodes as $dirty_child_node) {
                $this->sanitizeNode($healthy_node, $dirty_child_node);
            }
        }
    }
}
